
### Run
Just clone and load up in android studio.

## Lib Streaming
We are using [libstreaming](https://github.com/fyhertz/libstreaming) to handle Surface Preview Rendering, MediaCodec, and RTSP services.

We are using the `SurfaceView` class to view video preview and as a container, `MediaCodec` to got buffer, through the `h264` packetizer, and broadcasting over `RTSPServer`.

Here is the current `Session` configuration:

```
mSession = SessionBuilder.getInstance()
                .setSurfaceView(mSurfaceView)
                .setCallback(mSessionCallback)
                .setPreviewOrientation(0)
                .setContext(context)
                .setAudioEncoder(SessionBuilder.AUDIO_NONE)
                .setAudioQuality(AudioQuality(16000, 32000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
                .setVideoQuality(VideoQuality(2280,1080,30,6280000))
                .build()
```

*WARNING: specifying `0.0.0.0` as target IP address. This will allow anyone to setup a client and sniff our video. Need to change for production*
### Linking
*WARNING: linking documentation on libstreaming README is broke, use these setps instead*

[Reference](https://medium.com/@deepakpk/how-to-add-a-git-android-library-project-as-a-sub-module-c713a653ab1f) article on how to add git module into android app.

1. `git clone https://github.com/fyhertz/libstreaming.git` in the root of android application.
2. `File` -> `Project Structure` -> `+` top right corner -> `Import Gradle Project` --> add `libstreaming` folder
3. Add `include ':lib-streaming'` in `settings.gradle`
4. Add `compile project(':lib-streaming')` to `dependenceis { }` in `./app/build.gradle`
5. `Sync Gradle` button
6. `Build` hammer button

### Debugging Libstreaming
*WARNING: Libstreaming has dragons. High resolutions do not work out of the box. Many functions are broken... destruction, death, soldier on good sir.*

To debug specific components inside libstreaming search by `TAG` in `logcat`. For example, search `VideoQuality` for all logs emitting from that class.

Be mindful of the function `determineClosestSupportedResolution` in `VideoQuality.java`. It will attempt to modify your specified resolution with the closest supported resolution from the camera in use. NOTE: the library uses the older `Camera` api which has a smaller supported resoultion set than `Camera2` api.

*WARNING: the `testMediaCodecAPI` function in `H264Stream.java` forces use of the `MediaRecorder` api on high resolutions. Comment this fucker out!*

## Resolution

![Much Res](goat.png)

Current Resolution Set: `.setVideoQuality(VideoQuality(2280,1080,30,6280000))`

Supported Resolutions on `OnePluse6`:
*NOTE: we need to update for `OnePlus3` when we get one.*


```
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 4608x2304
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 4608x2592
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 4608x2176
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 4608x2112
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 4096x1940
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 4000x3000
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 3456x3456
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 3264x2448
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 3200x2400
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2976x2976
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2688x1512
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2592x1944
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2592x1940
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2340x1080
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2304x1728
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2280x1080
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2160x1080
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 2048x1536
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1920x1440
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1920x1080
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1440x1080
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1280x960
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1280x768
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1280x720
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1080x1080
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1024x738
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 1024x768
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 800x600
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 800x480
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 720x480
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 640x480
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 352x288
2018-11-09 20:04:02.239 14442-14442/biz.nycv.securitycamera E/CameraFragment: 320x240
2018-11-09 20:04:02.240 14442-14442/biz.nycv.securitycamera E/CameraFragment: 176x144

```  

For reference, [here](https://developer.android.com/guide/topics/media/media-formats) is a list of resolutions supported when using `MediaRecorder`
=======
# Testing

-Setup-

1) Recommendation: To avoid null video content during playback, make sure you are using an updated media player as default. Prefereably VLC.
To set this, open finder and select an MP4 file, select 'Get Info'. Scroll down to the 'Open With' selection and select 'VLC', the click 'Change All'.

2) Port forwarding. You will need to navigate to your ISP's router settings address and login to admin account. From here, you will need to configure the port forwarding for your device.




-Streaming-

Videos hosted from cloud server are currently configured to get pushed to 'fuse_dev' bucket.

Sorting through all these can be pain in the ass, so we can use a handy command to sort them for us and grab latest:

`gsutil ls -l gs://fuse_dev/ | sort -k 2`

-Debugging-

1) If video playback is in all black, check which media player you are using to playback the MP4. Try with VLC.
FFmpeg defaults to the least lossy color encoding for H264, which usually is YUV444p. This color encoding format is not supported on some outdated players such as Media Recorder.

2) If stream is not connecting, and you have been rapidly firing off tests, you may want to check the kubernetes pod.
    - `kubectl get pods`
You will want to look to see if container status is in "Crash Loop Backoff". This happens when rapid container shutdowns occur in sequence. After 10 mins, the container will restart itself.
>>>>>>> 8b25a4acb37631d67069394d118c6c0fb45caa1a

# Resources

-Android/Kotlin-

1. https://medium.com/thoughts-overflow/how-to-add-a-fragment-in-kotlin-way-73203c5a450b
- fragments the kotlin way

2. https://github.com/googlesamples/android-Camera2Basic/blob/master/kotlinApp/Application/src/main/java/com/example/android/camera2basic/CameraActivity.kt
- android camera kotlin example

3. https://www.raywenderlich.com/361-android-fragments-tutorial-an-introduction-with-kotlin
- interacting with fragments from activity (kotlin)

4. https://github.com/googlesamples/android-Camera2Video/blob/master/kotlinApp/Application/src/main/java/com/example/android/camera2video/Camera2VideoFragment.kt
- google sample video code
- MediaRecorder

-Video Encoding & Formats-

1) YUV Format
- https://en.wikipedia.org/wiki/YUV


2) H264/MPEG4
- https://en.wikipedia.org/wiki/H.264/MPEG-4_AVC

3) Open GL Tutorial
- http://www.opengl-tutorial.org/beginners-tutorials/tutorial-5-a-textured-cube/
