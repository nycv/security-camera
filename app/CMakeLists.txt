cmake_minimum_required(VERSION 2.8)
project( MotionDetection )

#add_executable( Processor ./cpp/Processor.cpp )

# add MotionDetector as a .so shared library
# TODO: need ot link libfastcv.a static library to MotionDetector shared library

# trying to include directory
include_directories(main/cpp)

find_library(
        # Defines the name of the path variable that stores the
        # location of the NDK library.
        log-lib

        # Specifies the name of the NDK library that
        # CMake needs to locate.
        log)

add_library(
        # name of library
        MotionDetector

        # shared or static
        SHARED

        # source file
        ${PROJECT_SOURCE_DIR}/src/main/cpp/MotionDetector.cpp)

# set version of Motion Detector
set_target_properties(MotionDetector PROPERTIES SOVERSION 0.0.1)


# declare the public API header file for MotionDetector
set_target_properties(MotionDetector PROPERTIES PUBLIC_HEADER ${PROJECT_SOURCE_DIR}/src/main/cpp/MotionDetector.h)

# link libfastcv.a static library to MotionDetector dynamic library
target_link_libraries(MotionDetector ${CMAKE_CURRENT_SOURCE_DIR}/src/main/cpp/libfastcv.a ${log-lib})



