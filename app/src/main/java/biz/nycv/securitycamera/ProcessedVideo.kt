package biz.nycv.securitycamera

import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.*

class ProcessedVideo : Fragment() {
    private val TAG = "ProcessedVideo"

    private lateinit var textureView: AutofitTexture

    private var backgroundThread: HandlerThread? = null

    private var backgroundHandler: Handler? = null

    private lateinit var previewSurface: Surface

    fun acceptNextFrame(frame: ByteArray) {
        // render image in background
        backgroundHandler?.post(ImageRenderer(frame, textureView))
    }

    fun configureTexture(width: Int, height: Int) {
        // make sure activity exists
        // if not, exit from function
        activity ?: return

        // grab orientation of fragment
        // should be in landscape (1)
        val rotation = (activity as FragmentActivity).windowManager.defaultDisplay.rotation


        // container for pixel matrix
        val matrix = Matrix()

        val viewRect = RectF(0f, 0f, width.toFloat(), height.toFloat())

        val bufferRect = RectF(0f, 0f, height.toFloat(), width.toFloat())

        val centerX = viewRect.centerX()
        val centerY = viewRect.centerY()

        bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())

        matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)

        val scale = Math.max(
                height.toFloat() / height,
                width.toFloat() / width)

        with(matrix) {
            postScale(scale, scale, centerX, centerY)
            postRotate((90 * (rotation - 2)).toFloat(), centerX, centerY)
        }

        Log.e(TAG, centerX.toString())
        Log.e(TAG, centerY.toString())

        // set transform
        textureView.setTransform(matrix)


    }

    fun startPreview() {
        val texture = textureView.surfaceTexture

        texture.setDefaultBufferSize(2074, 1080)

        previewSurface = Surface(texture)
    }

    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("ProcessedBackground").also { it.start() }
        backgroundHandler = Handler(backgroundThread?.looper)
    }

    /* Surface Texture Callback */

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {

        override fun onSurfaceTextureAvailable(texture: SurfaceTexture, width: Int, height: Int) {
            Log.d(TAG, "surface texture available")
            startBackgroundThread()
//            configureTexture(2074, 1080)
//            startPreview()
        }

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture, width: Int, height: Int) {
            Log.e(TAG, width.toString())
            Log.e(TAG, height.toString())
            Log.e(TAG, "surface texture size has changed")
//            configureTransform(width, height)
        }

        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture) = true

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture) = Unit

    }


    /* On Create Methods */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "in ProcessedVideo onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_processed_video, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "inside onViewCreated")
//        super.onViewCreated(view, savedInstanceState)
        textureView = view.findViewById(R.id.processedTexture)
        textureView.surfaceTextureListener = surfaceTextureListener
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                ProcessedVideo().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}
