package biz.nycv.securitycamera

import java.io.FileOutputStream
import java.io.OutputStreamWriter
import android.util.Log

internal class FfmpegPiper(
    private val mOut: FileOutputStream,
    private val h264: ByteArray,
    private val isClose: Boolean,
    private val fragmentListener: Camera.CameraBeacon?
): Runnable {
    val TAG = "FfmpegPiper"
    override fun run() {
        if (isClose) {
            try {
                mOut.flush()
                // wait for flush to finish before closing
            } finally {
                try {
                    mOut.close()
                    // wait for close to finish before reseting pipe
                } finally {
                    fragmentListener!!.resetFileStream()
                }
            }
        } else {
            Log.i(TAG, "writing data")
            mOut.write(h264)
        }
    }
}