package biz.nycv.securitycamera

import android.content.Context
import android.util.AttributeSet
import android.view.TextureView
import android.graphics.drawable.Drawable


/*    Do I really need this class? It really does not do anything.
    Setting styling in fragment_camera.xml and using it as a texture to render video preview*/

class AutofitTexture : TextureView {

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        val shape: Drawable? 
    }

}
