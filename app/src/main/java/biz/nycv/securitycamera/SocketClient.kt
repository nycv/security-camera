package biz.nycv.securitycamera

import android.util.Log
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.Exception
import java.net.*

class SocketClient(
    private val socketListener: SocketBeacon?
): Runnable {

    val TAG = "SocketClient"
    val serverIP: String = "192.168.1.3"
    lateinit var localIP: InetAddress
    val serverPort: Int = 8888
    lateinit var tcpClient: Socket

    override fun run() {
        Log.d(TAG, "inside socket client")

        // get local ip address from network state
        val lanIP = getLocalIPAddress()
        localIP = InetAddress.getByName(lanIP)

        // 1. attempt to connect to server
//        tcpClient = Socket(serverIP, serverPort, localIP, 8887)
        connectLoop()

        // 3. handle connected state
        Log.e(TAG, "connected to socket")
        try {
            var loop = true
            while (loop) {
                try {
                    // setup stream listener on socket
                    val message: BufferedReader = BufferedReader(InputStreamReader(tcpClient.getInputStream()))

                    // wait for message to finish
                    // !!! will block thread until line break of socket close
                    val line = message.readLine()

                    // turn input into JSON object
                    val jsonMessage = JSONObject(line)

                    // parse host and port
                    val host: String = jsonMessage.get("host").toString()
                    val port:String = jsonMessage.get("port").toString()

                    // communicate with FfmpegController
                    socketListener!!.sendFfmpegCommand(host, port)

                    // stop looping
                    loop = false
                } catch (error: Exception) {
                    Log.d(TAG, error.toString())

                    // close socket on error
                    tcpClient.close()
                } finally {
                    Log.d(TAG, "closing tcp client")

                    // closes socket when fineshed
                    tcpClient.close()
                }
            }
            Log.d(TAG, "while loop ended...")
        } catch (error: Exception) {
            Log.d(TAG, error.toString())
        }


    }

    /*
        NOT CONNECTED STATE
        attempt to connect every 500 miliseconds
     */
    fun connectLoop() {

        while (true) {
            // sleep for 500 miliseconds
            Thread.sleep(500)

            Log.i(TAG, "attempting to connect")

            try {
                // attempt to connect
                tcpClient = Socket(serverIP, serverPort, localIP, 8887)
            } catch (error: IOException) {
                // if cannot connect, wait and try again
                continue
            }

            // if connected, exit out of poll
            break
        }


        Log.i(TAG, "connected to relay server")
    }

    fun getLocalIPAddress(): String {
        // !!! not finished
        // need dynamically fetch LAN IP
        val interfaces: List<NetworkInterface> = NetworkInterface.getNetworkInterfaces().toList()
        for (interf in interfaces) {
            Log.e(TAG, interf.toString())
        }
        return "192.168.1.4"
    }


    interface SocketBeacon {
        fun sendFfmpegCommand(host: String, port: String)
    }
}