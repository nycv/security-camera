package biz.nycv.securitycamera

import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.YuvImage
import biz.nycv.securitycamera.Camera.CameraBeacon

import android.media.Image
import android.util.Log
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer
import java.util.*

internal class ImageProcessor(
        /**
         * The JPEG image
         */
        private val image: Image,
        private val motionDetector: MotionDetector,
        private val fragmentListener: CameraBeacon?
        /**
         * The file we save the image into.
         */
) : Runnable {
    override fun run() {
        // process frame
        // 1. process
        // 2. filter
        // 3. calculate
        Log.i(TAG, "0")
        processFrame(image, motionDetector)
        Log.i(TAG, "1")
        // end image session
        image.close()
    }

    private fun processFrame(image: Image, motionDetector: MotionDetector) {

        // get video dimensions
        val width: Int = image.width
        val height: Int = image.height

        // get Y plane
        val yBuff: ByteBuffer = image.planes[0].buffer
        var yBytes: ByteArray = ByteArray(yBuff.remaining())
        yBuff.get(yBytes)

        // get U plane
//        val uBuff: ByteBuffer = image.planes[1].buffer
//        var uBytes: ByteArray = ByteArray(uBuff.remaining())
//        uBuff.get(uBytes)

        // get V plane
//        val vBuff: ByteBuffer = image.planes[2].buffer
//        var vBytes: ByteArray = ByteArray(vBuff.remaining())
//        vBuff.get(vBytes)

        // empty y container
//        val yContainer: ByteArray = ByteArray(yBytes.size)

        // 1. process frame: blur -> frameDiff
//        val processedYBytes: ByteArray = motionDetector.processFrame(yBytes, width, height, yContainer, 640)
//        val processedYVU: ByteArray = YUV420toNV(processedYBytes, uBytes, vBytes)
//        val processedJPEG: ByteArray = NV21toJPEG(processedYVU, width, height)
//        fragmentListener!!.sendNextFrame(processedJPEG)
        Log.d(TAG, "detecting motion")
        val motionDetected: Boolean = motionDetector.detectMotion(yBytes, width, height)
        Log.e(TAG, motionDetected.toString())
        fragmentListener!!.setMotionDetected(motionDetected)

//        // 2. filter frame: threshold -> dilate
//        val filteredYBytes: ByteArray = motionDetector.thresholdFrame(processedYBytes, width, height, yContainer, yContainer)
//        val filteredYVU: ByteArray = YUV420toNV(filteredYBytes, uBytes, vBytes)
//        val filteredJPEG: ByteArray = NV21toJPEG(filteredYVU, width, height)
//        fragmentListener!!.sendFilteredFrame(filteredJPEG)

//        // 3. calc frame: contour -> area
//        Log.d(TAG, "calculating contour area...")
//        Log.e(TAG, filteredYBytes.size.toString())
//        val motionArea: Int = motionDetector.calculateMotionArea(filteredYBytes, width, height);
//        Log.e(TAG, motionArea.toString())

//        // 4. check motion: memory buffer -> average -> boolean
//        Log.d(TAG, "checking memory buffer")
//        val motionDetected: Boolean  = motionDetector.checkMemoryBuffer(motionArea)
//        Log.e(TAG, motionDetected.toString())

    }

    private fun YUV420toNV(y: ByteArray, u: ByteArray, v: ByteArray): ByteArray {
        // NV21 container
        var yvu: ByteArrayOutputStream = ByteArrayOutputStream()

        // write planes Y, V, U
        yvu.write(y)
        yvu.write(v)
        yvu.write(u)

        return yvu.toByteArray()
    }

    private fun NV21toJPEG(yvu: ByteArray, width: Int, height: Int): ByteArray {
        val yuvImage: YuvImage = YuvImage(yvu, ImageFormat.NV21, width, height, null)

        var bitmap: ByteArrayOutputStream = ByteArrayOutputStream()

        yuvImage.compressToJpeg(Rect(0, 0, width, height), 100, bitmap)

        return bitmap.toByteArray()
    }

    companion object {
        /**
         * Tag for the [Log].
         */
        private val TAG = "ImageProcessor"
    }
}





/*
    RESOURCES:

    https://en.wikipedia.org/wiki/YUV

 */