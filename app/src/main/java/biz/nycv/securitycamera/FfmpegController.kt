package biz.nycv.securitycamera

import android.util.Log

import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.Config

class FfmpegController(
    private var context: MainActivity,
    private var host: String,
    private var port: String,
    private val pipeToggle: Boolean
): Runnable {
    private val TAG = "FfmpegController"
    private var pipe1: String = ""

    override fun run() {
         startServer()
    }


    fun startServer() {
        var ffmpegCommand = ""
        /*

            1. toggle between mf_pipe_1 and mf_pipe_2
            2. toggle between client endpoints
            !!! Need to change endpoint handler in production

         */
        if (pipeToggle) {
            ffmpegCommand = "-loglevel debug -y -re -f h264 -i /data/data/biz.nycv.securitycamera/cache/mf_pipe_1 -f rtsp -rtsp_transport tcp rtsp://192.168.1.3:5558"
        } else {
            ffmpegCommand = "-loglevel debug -y -re -f h264 -i /data/data/biz.nycv.securitycamera/cache/mf_pipe_2 -f rtsp -rtsp_transport tcp rtsp://192.168.1.3:5559"
        }
//        val ffmpegCommand: String = "-y -f rawvideo -s 1280x720 -i $pipe1 -c:v mpeg4 /sdcard/test_here_2.mp4"
        FFmpeg.execute(ffmpegCommand, " ")
    }

    fun startLiveStream(host: String, port: String) {
        val ffmpegCommand: String = "-loglevel debug -y android_camera -framerate 30 -s 1280x720 -i discarded -c:v libx264 -preset ultrafast -f rtsp -rtsp_transport tcp rtsp://192.168.1.3:5558"
        FFmpeg.execute(ffmpegCommand, "")
    }

    fun checkStatus() {
        val rc: Int = FFmpeg.getLastReturnCode()
        Log.e(TAG, rc.toString())
        val outpout: String = FFmpeg.getLastCommandOutput()
        Log.e(TAG, outpout.toString())
    }

    companion object {
        /**
         * Tag for the [Log].
         */
        private val TAG = "ImageProcessor"
    }
}