package biz.nycv.securitycamera

import android.graphics.SurfaceTexture
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.Surface
import android.view.ViewGroup
import android.content.Context
import android.graphics.ImageFormat
import android.graphics.Matrix
import android.graphics.RectF
import android.hardware.camera2.*
import android.hardware.camera2.CameraCharacteristics.SENSOR_ORIENTATION
import android.hardware.camera2.CameraCaptureSession
import android.media.*
import java.util.concurrent.Semaphore
import android.os.Handler
import android.os.HandlerThread
import java.io.*
import java.lang.Exception
import java.lang.NullPointerException
import java.lang.StringBuilder
import java.math.BigInteger
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit

class Camera : Fragment() {
    /*
        Class variables
     */
    private val TAG = "Camera2"

    private lateinit var textureView: AutofitTexture

    private lateinit var manager: CameraManager

    private var textureWidth = 0

    private var textureHeight = 0

    private var cameraId = ""

    private val cameraLock = Semaphore(1)

    private lateinit var previewRequestBuilder: CaptureRequest.Builder

    private var cameraDevice: CameraDevice? = null

    private var captureSession: CameraCaptureSession? = null

    private val STATE_PREVIEW = 0

    private var state = STATE_PREVIEW

    private var imageReader: ImageReader? = null

    private var backgroundHandler: Handler? = null

    private var backgroundThread: HandlerThread? = null

    private var backgroundHandler2: Handler? = null

    private var backgroundThread2: HandlerThread? = null

    private var backgroundHandler3: Handler? = null

    private var backgroundThread3: HandlerThread? = null

    private var sleepHandler: Handler? = null

    private var sleepThread: HandlerThread? = null

    private var writerHandler: Handler? = null

    private var writerThread: HandlerThread? = null

    private var processorHandler: Handler? = null

    private var processorThread: HandlerThread? = null

    private lateinit var motionDetector: MotionDetector

    private var counter = 0

    private lateinit var cameraModel: CameraModel

    private var fragmentListener: CameraBeacon? = null

    private lateinit var mEncoder: MediaCodec

    private lateinit var mRecordingSurface: Surface

    private lateinit var mBufferInfo: MediaCodec.BufferInfo

    private var motionDetected: Boolean = false

    private lateinit var mOut: OutputStreamWriter

    private lateinit var mOut2: FileOutputStream

    private var prevMotionDetected = false

    private var isFirstFrame = true

    private var isSecondFrame = false

    private lateinit var keyFrame: ByteArray

    private var isFirstMotionFrame = true

    private lateinit var keyFrame2: ByteArray

    private var pipeFileToggle: Boolean = true

    /*
        CAMERA Utils
     */

    private fun startCamera() {
        // TODO: check permissions

        // reference to the parent activity
        val mainActivity = activity

        // handle race condition with camera lock
        if (!cameraLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
            throw RuntimeException("Time out waiting to lock camera opening.")
        }

        // create camera manager
        // https://developer.android.com/reference/android/hardware/camera2/CameraManager
        manager = mainActivity!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager

        // camera id 0 is the back camera
        cameraId = manager.cameraIdList[0]

        // fetch camera characteristics
        getCameraCharacteristics(cameraId)

        configureTransform(1280, 720)

        manager.openCamera(cameraId, cameraCallback, backgroundHandler)

    }

    private fun getCameraCharacteristics(cameraId: String) {
        val characteristics = manager.getCameraCharacteristics(cameraId)
        Log.e(TAG, characteristics.toString())

        val sensorOrientation = characteristics.get(SENSOR_ORIENTATION)
        Log.e(TAG, sensorOrientation.toString())

        Log.d(TAG, "before image reader instantiation")
        imageReader = ImageReader.newInstance(
                1280,
                720, ImageFormat.YUV_420_888,
                10).apply {
                    setOnImageAvailableListener(imageReaderCallback, backgroundHandler3)
                }

        Log.d(TAG, "after image reader instantiation")
        configureTransform(1280, 720)
    }

    private fun configureTransform(width: Int, height: Int) {
        // make sure activity exists
        // if not, exit from function
        // TODO: make error log if actiivty does not exist
        activity ?: return

        // grab orientation of fragment
        // should be in landscape (1)
        val rotation = (activity as FragmentActivity).windowManager.defaultDisplay.rotation

        // container for pixel matrix
        val matrix = Matrix()

        val viewRect = RectF(0f, 0f, width.toFloat(), height.toFloat())

        val bufferRect = RectF(0f, 0f, height.toFloat(), width.toFloat())

        val centerX = viewRect.centerX()
        val centerY = viewRect.centerY()

        bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
        matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
        val scale = Math.max(
                height.toFloat() / height,
                width.toFloat() / width)
        with(matrix) {
            postScale(scale, scale, centerX, centerY)
            postRotate((90 * (rotation - 2)).toFloat(), centerX, centerY)
        }

        // set transform
        textureView.setTransform(matrix)

    }

    private fun startPreview() {
        try {
            val texture = textureView.surfaceTexture

            texture.setDefaultBufferSize(1280, 720)


            val previewSurface = Surface(texture)

            Log.d(TAG, "starting preview")

            // !!! WARNING: this seems to be broken on onpelus3
            previewRequestBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            previewRequestBuilder.addTarget(mRecordingSurface)
            previewRequestBuilder.addTarget(previewSurface)
            previewRequestBuilder.addTarget(imageReader?.surface)


            cameraDevice?.createCaptureSession(listOf(previewSurface, mRecordingSurface, imageReader?.surface), previewCallback, backgroundHandler)

        } catch (e: CameraAccessException) {
            Log.e(TAG, "error in start preview")
            Log.e(TAG, e.toString())
        }
    }

    private fun updatePreview() {
        if (cameraDevice == null) Log.e(TAG, "camera device is null")
        try {
            setupCaptureRequestBuilder(previewRequestBuilder)
            captureSession?.setRepeatingRequest(previewRequestBuilder.build(), captureCallback, backgroundHandler2)
        } catch (e: CameraAccessException) {
            Log.e(TAG, "error in update preview")
            Log.e(TAG, e.toString())
        }
    }

    private fun setupCaptureRequestBuilder(builder: CaptureRequest.Builder?) {
        builder?.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
    }

    private fun configureEncoder() {
        mBufferInfo = MediaCodec.BufferInfo()

        // set media format constants
        var format: MediaFormat = MediaFormat.createVideoFormat("video/avc", 1280, 720)

        // configure media codec settings
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface)

        format.setInteger(MediaFormat.KEY_BIT_RATE, 3000 * 1000)

        format.setInteger(MediaFormat.KEY_FRAME_RATE, 30)

        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1)

        format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 1280*720)


        // create encoder with mime type "video/mp4"
        try {
            mEncoder = MediaCodec.createEncoderByType("video/avc")
        } catch (error: Exception) {

            when (error) {
                is IOException -> Log.d(TAG, "IOExecption") // cannot use mime type
                is IllegalArgumentException -> Log.d(TAG, "IllegalArgumentException") // incorrect mime type
                is NullPointerException -> Log.d(TAG, "NullPointerException") // null mime type
            }
        }


        // configure encoder with formatting
        try {
            mEncoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
        } catch (error: Exception) {
            when (error) {
                is IllegalArgumentException -> Log.d(TAG, "surface has is released of not CONFIGURE_FLAG_ENCODE")
                is IllegalStateException -> Log.d(TAG, "if not in Unitialized state")
                is MediaCodec.CryptoException -> Log.d(TAG, "DRM error")
                is MediaCodec.CodecException -> Log.d(TAG, "codec error")
            }
        }

        // create surface data type to hardware accelerate encoding
        mRecordingSurface = mEncoder.createInputSurface()

        // setup callback to retrieve buffers asynchronously
        mEncoder.setCallback(mediaCodecCallback)

    }

    private fun setKeyFrame(keyFrameBuff: ByteBuffer) {
        // flip first frame toggle
        isFirstFrame = false

        // flip second frame toggle
        isSecondFrame = true

        // convert keyframe from ByteBuffer to ByteArray
        val convertedKeyFrame = ByteArray(keyFrameBuff.remaining())
        keyFrameBuff.get(convertedKeyFrame)

        // save to state variable
        keyFrame = convertedKeyFrame
    }

    private fun setKeyFrame2(keyFrameBuff: ByteBuffer) {
        Log.i(TAG, "inside set keyFrame2")
        // flip second frame toggle
        isSecondFrame = false

        // convert keyframe from ByteBuffer to ByteArray
        val convertedKeyFrame = ByteArray(keyFrameBuff.remaining())
        keyFrameBuff.get(convertedKeyFrame)

        // save to state variable
        keyFrame2 = convertedKeyFrame
    }

    private fun release() {
        // TODO: handle surface exit
        // NOTE: check out Persistent Surface for handling this automatically
        mEncoder.stop()
        mEncoder.release()
    }

    fun startPipeListener() {
        // create file stream pointed at mkfifo pipe file
        if (pipeFileToggle) {
            mOut2 = FileOutputStream("/data/data/biz.nycv.securitycamera/cache/mf_pipe_1")
        } else {
            mOut2 = FileOutputStream("/data/data/biz.nycv.securitycamera/cache/mf_pipe_2")
        }

    }

    /*
        Camera Callback
     */

    private val cameraCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cameraDevice: CameraDevice) {
            Log.d(TAG, "camera is opened")

            // open camera lockz
            cameraLock.release()
            this@Camera.cameraDevice = cameraDevice
            // TODO: start preview
            startPreview()
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            Log.d(TAG, " camera disconnected")
            cameraLock.release()
            cameraDevice.close()
            this@Camera.cameraDevice = null
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            Log.d(TAG, "error in camera")
            cameraLock.release()
            cameraDevice.close()
            activity?.finish()
        }
    }

    /*
        Preview Callback
     */

    private val previewCallback = object : CameraCaptureSession.StateCallback() {
        override fun onConfigured(session: CameraCaptureSession) {
            Log.d(TAG, "preview has been configured")
            captureSession = session
            updatePreview()

        }

        override fun onConfigureFailed(session: CameraCaptureSession) {
            Log.d(TAG, "preview configuration failed")
        }
    }

    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("CameraBackground").also { it.start() }
        backgroundHandler = Handler(backgroundThread?.looper)
    }

    private fun startBackgroundThread2() {
        backgroundThread2 = HandlerThread("BackgroundThread2").also { it.start() }
        backgroundHandler2 = Handler(backgroundThread2?.looper)
    }

    private fun startBackgroundThread3() {
        backgroundThread3 = HandlerThread("BackgroundThread3").also { it.start() }
        backgroundHandler3 = Handler(backgroundThread3?.looper)
    }

    private fun startSleepThread() {
        sleepThread = HandlerThread("SleepThread").also { it.start() }
        sleepHandler = Handler(sleepThread?.looper)
    }

    private fun startWriterThread() {
        writerThread = HandlerThread("WriterThread").also { it.start() }
        writerHandler = Handler(writerThread?.looper)
    }

    private fun startProcessorThread() {
        processorThread = HandlerThread("ProcessorThread").also { it.start() }
        processorHandler = Handler(processorThread?.looper)
    }

    /*
        SurfaceTexture Callback
     */

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {

        override fun onSurfaceTextureAvailable(texture: SurfaceTexture, width: Int, height: Int) {
            Log.d(TAG, "surface texture available")

            // save width and height of texture
            textureWidth = width
            textureHeight = height

            // start camera thread
            startBackgroundThread()
            startBackgroundThread2()
            startBackgroundThread3()

            // start sleeper thread
            startSleepThread()

            // start writer thread
            startWriterThread()

            // start processor thread
            startProcessorThread()

            Log.i(TAG, "about to configure IO stream")

            // fetch the I/O reference
            // !!! hits main activity (see fragmentListener)
//            fragmentListener!!.shareIOReference()

            // configure encoder
            configureEncoder()

            // boot up encoder
            mEncoder.start()

            // start camera open when texture is available
            startCamera()

        }

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture, width: Int, height: Int) {
            Log.e(TAG, "surface texture size has changed")
//            configureTransform(width, height)
        }

        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture) = true

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture) = Unit

    }

    /*
        Capture Callback
     */

    private val captureCallback = object : CameraCaptureSession.CaptureCallback() {
        private fun process(result: CaptureResult) {
            Log.e(TAG, "in process in captureCallback")
            when (state) { STATE_PREVIEW -> Unit }

        }
    }

    /*
        Image Reader Callback
     */

    private val imageReaderCallback = ImageReader.OnImageAvailableListener {
//        Log.d(TAG, "inside image reader callback")
        processorHandler?.post(ImageProcessor(it.acquireNextImage(), motionDetector, fragmentListener))
    }

    /*
        MediaCodec Callback
    */
    private val mediaCodecCallback = object : MediaCodec.Callback() {
        override fun onInputBufferAvailable(mc: MediaCodec, inputBufferId: Int) {
            /* N/A */
            val inputBuffer: ByteBuffer = mEncoder.getInputBuffer(inputBufferId)
        }

        override fun onOutputBufferAvailable(mc: MediaCodec, outputBufferId: Int, bufferInfo: MediaCodec.BufferInfo) {
            // get media format for encoded buffer
            val bufferFormat: MediaFormat = mEncoder.getOutputFormat(outputBufferId)

            // get output buffer from queue
            val outputBuffer: ByteBuffer = mEncoder.getOutputBuffer(outputBufferId)

            if (isFirstFrame) setKeyFrame(outputBuffer)

            if (isSecondFrame) setKeyFrame2(outputBuffer)

            // !!! only stream out when motion is detected
            if (motionDetected) {
                Log.i(TAG, "motion detected")

                // flip prevMotionDetected sempaphore
                prevMotionDetected = true

                if (isFirstMotionFrame) {

                    writerHandler?.post(FfmpegPiper(mOut2, keyFrame, false, fragmentListener))
                    writerHandler?.post(FfmpegPiper(mOut2, keyFrame2, false, fragmentListener))
                    isFirstMotionFrame = false
                }

                // !!! convert ByteBuffer to ByteArray
                val convertedBuff = ByteArray(outputBuffer.remaining())
                outputBuffer.get(convertedBuff)

                // get hex dump
//                val formatter = Formatter()
//                for (b in convertedBuff) {
////                    binaryString.append(Integer.toBinaryString(b.toInt()))
//                    formatter.format("%02x", b)
////                    val adjusted = b.toInt() and 0xff
//                }
//
//                val formatString = formatter.toString()
//
//                Log.i(TAG, formatString)

                // write to file stream
                // !!! FfmpegController is listening on the other side

                writerHandler?.post(FfmpegPiper(mOut2, convertedBuff, false, fragmentListener))

            } else {
                if (prevMotionDetected == true) {
                    Log.i(TAG, "reset motion clip")

                    // reset first motion frame semaphore
                    isFirstMotionFrame = true

                    // reset motion detected semaphore
                    prevMotionDetected = false

                    // flip pipe toggle
                    pipeFileToggle = !pipeFileToggle

                    // flush and close file stream
                    writerHandler?.post(FfmpegPiper(mOut2, ByteArray(1), true, fragmentListener))
                }
            }

            // release codec buffer
            mEncoder.releaseOutputBuffer(outputBufferId, true)
        }

        override fun onOutputFormatChanged(codec: MediaCodec, format: MediaFormat) {
            Log.d(TAG, "output format has changed")
        }

        override fun onError(mc: MediaCodec, error: MediaCodec.CodecException) {
            Log.e(TAG, "error in mEncoder")
        }
    }

    /*
        ON CREATE METHODS
     */

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_camera2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        textureView = view.findViewById(R.id.texture)
        textureView.surfaceTextureListener = surfaceTextureListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // load cpp interface
        motionDetector = MotionDetector()

        // !!! this seems to break on oneplus devices. needs further investigation
        // make fastcv run on dsp
        motionDetector.targetDSP()

        startPipeListener()

    }

    override fun onDetach() {
        super.onDetach()
        fragmentListener = null
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CameraBeacon) fragmentListener = context
    }

    // fragment instantiation
    companion object {
        @JvmStatic
        fun newInstance() =
                Camera().apply {
                    arguments = Bundle().apply {

                    }
                }
    }

    /* Fragment -> Activity Interface */

    interface CameraBeacon {
        fun setPreviousFrame(frame: ByteArray)
        fun setMotionArea(area: Int)
        fun setMotionDetected(motionDetected: Boolean)
        fun resetFileStream()
        fun restartFfmpeg()

//        fun sendFilteredFrame(frame: ByteArray)
//        fun sendNextFrame(frame: ByteArray)
    }

    /* Activity -> Fragment Interface */

    fun acceptMotionState(motionState: Boolean) {
        motionDetected = motionState
    }

    fun receiveIOFile(out: OutputStreamWriter) {
        // save reference to
        mOut = out
    }

    fun reopenPipe() {
        startPipeListener()
    }
}
