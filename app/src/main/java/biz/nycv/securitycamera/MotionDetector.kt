package biz.nycv.securitycamera


class MotionDetector {
    private val TAG = "MotionDetector"

    @Throws(IllegalArgumentException::class)
    external fun setCounter(counter: Int): Int

    @Throws(IllegalArgumentException::class)
    external fun getCounter(): Int

    @Throws(IllegalArgumentException::class)
    external fun processFrame(
            sourceFrame: ByteArray,
            frameWidth: Int,
            frameHeight: Int,
            destinationFrame: ByteArray,
            stride: Int): ByteArray


    @Throws(java.lang.IllegalArgumentException::class)
    external fun thresholdFrame(
            sourceFrame: ByteArray,
            frameWidth: Int,
            frameHeight: Int,
            filteredFrame: ByteArray,
            destinationFrame: ByteArray): ByteArray

    @Throws(java.lang.IllegalArgumentException::class)
    external fun calculateMotionArea(
            sourceFrame: ByteArray,
            frameWidth: Int,
            frameHeight: Int): Int

    @Throws(java.lang.IllegalArgumentException::class)
    external fun checkMemoryBuffer(
            motionArea: Int): Boolean

    @Throws(IllegalArgumentException::class)
    external fun yuv420toRGB(
            sourceFrame: ByteArray,
            frameWidth: Int,
            frameHeight: Int,
            destinationFrame: ByteArray): ByteArray

    @Throws(IllegalArgumentException::class)
    external fun detectMotion(
            sourceFrame: ByteArray,
            frameWidth: Int,
            frameHeight: Int): Boolean

    @Throws(java.lang.IllegalArgumentException::class)
    external fun targetDSP()

    /*
       =============
     */

    companion object {
        init {
            System.loadLibrary("MotionDetector")
        }
    }

}
