package biz.nycv.securitycamera

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.ClipData.Item


class CameraModel : ViewModel() {
    var counter = MutableLiveData<Int>()
    var frame = MutableLiveData<ByteArray>()

    fun updateCounter(newCounter: Int) {
        counter.value = newCounter
    }

    fun updateFrame(newFrame: ByteArray) {
        frame.value = newFrame
    }
}