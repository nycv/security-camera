package biz.nycv.securitycamera
import java.net.ServerSocket
import android.util.Log
import java.io.IOException
import java.net.BindException

class RTSPServer {
    private val TAG = "RTSPServer"

    private val DEFAULT_RTSP_PORT = 5555

    private lateinit var mServer: ServerSocket


    fun startServer() {
        try {
            mServer = ServerSocket(DEFAULT_RTSP_PORT)
            start()
        } catch (error: Exception) {
            when (error) {
                is BindException -> Log.d(TAG, "port already in use")
                is IOException -> Log.d(TAG, "error instantiating server")
            }
        }

    }

    private fun startBackroundThread() {

    }

    private fun start() {
        while (true) {
            // accept in background thread
        }
    }

    fun kill() {
        mServer.close()
    }

}