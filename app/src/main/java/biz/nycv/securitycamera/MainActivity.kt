package biz.nycv.securitycamera

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.view.View
import android.support.v4.app.ActivityCompat
import android.Manifest
import android.graphics.Color
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter

class MainActivity: Camera.CameraBeacon, SocketClient.SocketBeacon, AppCompatActivity() {
    private val TAG = "MainActivity"

    // initiate Fragments
    // uncomment processedVideo fragments for CV testing
    private var processedVideo: ProcessedVideo = ProcessedVideo.newInstance()
    private var processedVideo2: ProcessedVideo = ProcessedVideo.newInstance()
    private var camera: Camera = Camera.newInstance()


    private val RECORD_REQUEST_CODE = 101

    // !!! state variable
    private lateinit var previousFrame: ByteArray

    private lateinit var ffmpegThread: HandlerThread
    private lateinit var ffmpegHandler: Handler

    private lateinit var socketThread: HandlerThread
    private lateinit var socketHandler: Handler

    private lateinit var ioThread: HandlerThread
    private lateinit var ioHandler: Handler

    private lateinit var mOut: OutputStreamWriter

    private var pipeToggle: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 1. clear window
        cleanWindow()

        // 2. make permissions requests
        makePermissionRequests()

        // 4. hydrate view with MainActivity
        setContentView(R.layout.activity_main)

        // !!! socket is currently disabled
        // NOTE: needed for live stream initialization
//        startSocketThread()
//        socketHandler?.post(SocketClient(this))

        // execute Ffmpeg command
        // backend for FfempgSteram
        // !!! this runs in a background since it Ffmpeg cmd is listening which halts thread
        startFfmpegThread()
        ffmpegHandler?.post(FfmpegController(this, "", "", true))

        // 5. add Camera and ProcessedVideo to context
        savedInstanceState ?: supportFragmentManager.beginTransaction()
                .add(R.id.camera, camera)
                .add(R.id.processed, processedVideo)
                .add(R.id.processed2, processedVideo2)
                .commit()
    }

    private fun makePermissionRequests() {
        /*
            1. CAMERA
            2. WRITE_EXTERNAL_STORAGE
            3. RECORD_AUDIO
        */
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO),
                RECORD_REQUEST_CODE)
    }

    private fun cleanWindow() {
        // remove top bar from app
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)

        window.decorView.apply {
            // Hide both the navigation bar and the status bar.
            systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        // set to full screen programmatically
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // enable hardware acceleration for Camera2
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        // set status bar to transparent
        getWindow().setStatusBarColor(Color.TRANSPARENT)

        // set navigation bar to black
        getWindow().setNavigationBarColor(Color.BLACK)
    }

    private fun startFfmpegThread() {
        ffmpegThread = HandlerThread("FFmpegThread").also { it.start() }
        ffmpegHandler = Handler(ffmpegThread?.looper)
    }

    private fun startSocketThread() {
        socketThread = HandlerThread("SocketThread").also { it.start() }
        socketHandler = Handler(socketThread?.looper)
    }

    /* Camera Fragment Interface */

    //  !!! removing testing tiles for now
//    override fun sendNextFrame(frame: ByteArray) {
//        Log.d(TAG, "inside send Next Frame in main activity")
//        Log.e(TAG, frame.toString())
//
//        // send frame to ProcessedVideo fragment
//        processedVideo!!.acceptNextFrame(frame)
//    }
//
//    override fun sendFilteredFrame(frame: ByteArray) {
//        Log.d(TAG, " inside send filtered frame")
//        Log.e(TAG, frame.toString())
//
//        processedVideo2!!.acceptNextFrame(frame)
//    }

    fun shareIOReference() {
        Log.i(TAG, "insid  shareIOReference")
        camera!!.receiveIOFile(mOut)
    }

    override fun setPreviousFrame(frame: ByteArray) {
        Log.d(TAG, "inside setPreviousFrame")
        Log.e(TAG, frame.toString())

        previousFrame = frame
    }

    override fun setMotionArea(area: Int) {
        Log.d(TAG, "inside setMotionArea")
        Log.d(TAG, area.toString())
    }


    override fun setMotionDetected(motionDetected: Boolean) {
        // TODO: need to commmunicate back to Camera fragment
        camera!!.acceptMotionState(motionDetected)
    }

    // ffmpeg initialization
    override fun sendFfmpegCommand(host: String, port: String) {
        startFfmpegThread()

        ffmpegHandler?.post(FfmpegController(this, host, port, true))

    }

    override fun resetFileStream() {
        // toggle pipe reference
        pipeToggle = !pipeToggle

        // dispatch new ffmpeg process
        ffmpegHandler?.post(FfmpegController(this, "", "", pipeToggle))

        // initate pipe reference update in Camera
        camera!!.startPipeListener()
    }

    override fun restartFfmpeg() {
        Log.i(TAG, "restarting ffmpeg")
    }

}
