package biz.nycv.securitycamera

import android.app.Activity
import android.util.Log
import java.io.*

class IOStream(
    context: Activity
): Runnable {
    val TAG = "IOStream"
    lateinit var mOut: OutputStreamWriter
    lateinit var foobar: FileOutputStream

    override fun run() {

        val path = "/data/data/biz.nycv.securitycamera/cache/mf_pipe_2"
        // configure IO file
        try {
            Log.i(TAG, "0")
            foobar = FileOutputStream(path)
//            context!!.shareIOReference()
            Log.i(TAG, "1")
        } catch (error: FileNotFoundException) {
            Log.e(TAG, "Error: file not found exception")
            error.printStackTrace()
        } catch (error: IOException) {
            Log.e(TAG, "Error: during writing")
        } finally {
            Log.i(TAG, "IO stream setup at $path")
        }
    }

    private fun pipeData(byteArray: ByteArray) {
        foobar.write(byteArray)
    }
}