package biz.nycv.securitycamera

import android.graphics.*
import java.io.ByteArrayOutputStream
import android.util.Log

internal class ImageRenderer (
    private val image: ByteArray,
    private val textureView: AutofitTexture
) : Runnable {

    override fun run() {
        // !!! hard coded for now
        val width: Int = 640
        val height: Int = 480

        // Convert NV21 byte array to JPEG format
        // https://developer.android.com/reference/android/graphics/ImageFormat
//        var jpeg = NV21toJPEG(image, width, height)

        // create bitmap from jpeg image
        val bitmap: Bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, null)

        // render bitmap to canvas
        val paint = Paint()
        val canvas = textureView.lockCanvas()
        canvas.drawBitmap(bitmap, 0f, 0f, paint)
        textureView.unlockCanvasAndPost(canvas)
    }


    private fun NV21toJPEG(image: ByteArray, width: Int, height: Int): ByteArray {
        var out: ByteArrayOutputStream = ByteArrayOutputStream()

        var yuv: YuvImage = YuvImage(image, ImageFormat.NV21, width, height, null)
        yuv.compressToJpeg(Rect(0, 0, width, height), 1, out)

        return out.toByteArray()
    }


    companion object {
        private val TAG = "ImageRenderer"
    }
}