//
// Created by Sean Pollock on 11/17/18.
//

#ifndef SECURITYCAMERA_MOTIONDETECTOR_H
#define SECURITYCAMERA_MOTIONDETECTOR_H

#include <jni.h>
#include <deque>

class MotionDetector {
    // frame counter from app launch
    long counter;

    // previous frame cache for image diffing
    uint8_t* previousFrame;

    // memory container for motion detection smoothing
    std::deque <int> memoryBuffer;

public:
    MotionDetector() { counter = 0; }
    void targetDSP();
    long setCounter(int newCounter);
    long getCounter();
    bool detectMotion(uint8_t* sourceFrame, uint8_t* blurFrame, uint8_t* diffFrame, uint8_t* filteredFrame, uint8_t* dilatedFrame, unsigned int width, unsigned int height, unsigned int frameLength);
    uint8_t* processFrame(uint8_t* sourceFrame, uint8_t* blurFrame, unsigned int width, unsigned int height, uint8_t* destinationFrame, unsigned int stride);
    uint8_t* thresholdFrame(uint8_t* sourceFrame, unsigned int width, unsigned int height, uint8_t* filteredFrame, uint8_t* dilatedFrame);
    unsigned int calculateMotionArea(uint8_t* sourceFrame, unsigned int width, unsigned int height, int frameLength, unsigned int pixelIntensity);
    bool checkMemoryBuffer(int motionArea);
    int calculateMovingAverage();
    uint32_t* yuv420toRGB(uint8_t* sourceFrame, unsigned int width, unsigned int  height, uint32_t* destinationFrame);
};

#endif //SECURITYCAMERA_MOTIONDETECTOR_H
