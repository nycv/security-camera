//
// Created by Sean Pollock on 11/17/18.
//

#include "MotionDetector.h"

#include <android/log.h>
#include <jni.h>
#include <exception>
#include "fastcv.h"
#include <string>

// Set DSP target (defaults to CPU)
void MotionDetector::targetDSP() {
    fcvSetOperationMode(FASTCV_OP_PERFORMANCE);
}

long MotionDetector::setCounter(int newCounter) {
    counter = newCounter;
    return counter;
}

long MotionDetector::getCounter() { return counter; }

bool MotionDetector::detectMotion(uint8_t *sourceFrame, uint8_t *blurFrame, uint8_t *diffFrame,
                                  uint8_t *filteredFrame, uint8_t *dilatedFrame, unsigned int width,
                                  unsigned int height, unsigned int frameLength) {

    // 1. frame blur + diff
    uint8_t* processedFrame = processFrame(sourceFrame, blurFrame, width, height, diffFrame, 1280);

    // 2. threshold + dilate
    uint8_t* thresholdedFrame = thresholdFrame(processedFrame, width, height, filteredFrame, dilatedFrame);

    // 3. calc motion area
    unsigned int motionArea = calculateMotionArea(thresholdedFrame, width, height, frameLength, 255);

    // 4. take average of memory buffer
    bool motionDetected = checkMemoryBuffer(motionArea);

    return motionDetected;
}

uint8_t* MotionDetector::processFrame(uint8_t* sourceFrame, uint8_t* blurFrame, unsigned int width, unsigned int height, uint8_t* destinationFrame, unsigned int stride) {
    // blur frame
    fcvFilterGaussian11x11u8_v2(sourceFrame, width, height, width, blurFrame, width, 10);

    // handle first frame
    if (!previousFrame) {
        previousFrame = blurFrame;
        return blurFrame;
    }

    // take difference between previous and current frame
    fcvImageDiffu8_v2(previousFrame, blurFrame, width, height, 1280, destinationFrame, 1280);
    previousFrame = blurFrame;

    return destinationFrame;
}

uint8_t* MotionDetector::thresholdFrame(uint8_t *sourceFrame, unsigned int width, unsigned int height, uint8_t *filteredFrame, uint8_t *dilatedFrame) {

    // threshold filter
    fcvFilterThresholdu8_v3(sourceFrame, width, height, 1280, filteredFrame, 1280, 100, 255, 0);

    // dilate the filtered plane
    fcvFilterDilate3x3u8_v3(filteredFrame, width, height, width, dilatedFrame, width, FASTCV_BORDER_CONSTANT, 0);

    return dilatedFrame;
}

unsigned int MotionDetector::calculateMotionArea(uint8_t *sourceFrame, unsigned int width, unsigned int height, int frameLength, unsigned int pixelIntensity) {
    // white pixel counter
    unsigned int totalWhitePixels = 0;

    // iterate through and print frame bytes
    for (unsigned int i = 0; i < frameLength; i = i +1) {
//        __android_log_print(ANDROID_LOG_DEBUG, "MotionDetector", "%u", sourceFrame[i]);

        // if pixel is white, add to tally
        if (sourceFrame[i] == pixelIntensity) totalWhitePixels += 1;
    }

    return totalWhitePixels ;
}

/*

    max_length = 30

*/
bool MotionDetector::checkMemoryBuffer(int motionArea) {
    // push motion area onto memory
    memoryBuffer.push_back(motionArea);

    // check if memory buffer is full
    if (memoryBuffer.size() > 90) {
        memoryBuffer.pop_front();

        // take moving average motion area
        int summedArea = 0;

        for (int i = 0; i < memoryBuffer.size(); i++) {
            summedArea += memoryBuffer[i];
        }

        int average = summedArea / memoryBuffer.size();

        __android_log_print(ANDROID_LOG_DEBUG, "MotionDetector", "%i", average);

        // check if average is greater than some threshold
        if (average > 90000) return true;
        else return false;
    }

    // if memory buffer is not full, disable detection
    return false;
}


uint32_t* MotionDetector::yuv420toRGB(uint8_t *sourceFrame, unsigned int width, unsigned int height, uint32_t *destinationFrame) {
    fcvColorYUV420toRGB8888u8(sourceFrame, width, height, destinationFrame);

    return destinationFrame;
}

MotionDetector motionDetector;


// JNI Interface

// setFoo handler
extern "C"
JNIEXPORT jint JNICALL
Java_biz_nycv_securitycamera_MotionDetector_setCounter(
        JNIEnv* pEnv,
        jobject pThis,
        jint counter) {
    return motionDetector.setCounter(counter);
}

// getFoo JNI handler
extern "C"
JNIEXPORT jint JNICALL
Java_biz_nycv_securitycamera_MotionDetector_getCounter(
        JNIEnv* pEnv,
        jobject pThis) {
    return motionDetector.getCounter();
}

// Detect Motion Interface
extern "C"
JNIEXPORT jboolean JNICALL
Java_biz_nycv_securitycamera_MotionDetector_detectMotion(
    JNIEnv* pEnv,
    jobject pThis,
    jbyteArray sourceFrame,
    jint width,
    jint height) {

    // convert jbyte array into cpp uint8_t* array
    int frameLength = pEnv->GetArrayLength(sourceFrame);
    void* convertedSourceFrame = fcvMemAlloc(height * width, 16);
    pEnv->GetByteArrayRegion(sourceFrame, 0, frameLength, reinterpret_cast<jbyte*>(convertedSourceFrame));

    // create empty frame containers
    void* memDiffFrame = fcvMemAlloc(height * width, 16);
    void* memBlurFrame = fcvMemAlloc(height * width, 16);
    void* memFilteredFrame = fcvMemAlloc(height * width, 16);
    void* memDilatedFrame = fcvMemAlloc(height * width, 16);

    // call motion detect
    bool motionDetected = motionDetector.detectMotion(
            (uint8_t*)convertedSourceFrame, (uint8_t*)memBlurFrame,
            (uint8_t*)memDiffFrame, (uint8_t*)memFilteredFrame,
            (uint8_t*)memDilatedFrame,
            width, height, frameLength);

    fcvMemFree(memDiffFrame);
    fcvMemFree(memBlurFrame);
    fcvMemFree(memFilteredFrame);
    fcvMemFree(memDilatedFrame);
    fcvMemFree(convertedSourceFrame);

    // return boolean value
    return motionDetected;
}


extern "C"
JNIEXPORT jbyteArray JNICALL
Java_biz_nycv_securitycamera_MotionDetector_thresholdFrame(
        JNIEnv* pEnv,
        jobject pThis,
        jbyteArray sourceFrame,
        jint width,
        jint height,
        jbyteArray filteredFrame,
        jbyteArray destinationFrame) {

    // convert jbyteArray to cpp uint8_t*
    int frameLength = pEnv->GetArrayLength(sourceFrame);
    uint8_t* convertedSourceFrame = new uint8_t[frameLength];
    pEnv->GetByteArrayRegion(sourceFrame, 0, frameLength, reinterpret_cast<jbyte*>(convertedSourceFrame));
    uint8_t* convertedFilteredFrame = new uint8_t[frameLength];
    uint8_t* convertedDestinationFrame = new uint8_t[frameLength];

    uint8_t* processedFrame = motionDetector.thresholdFrame(convertedSourceFrame, width, height, convertedFilteredFrame, convertedFilteredFrame);

    // convert uint8_t* to jbyteArray
    jbyteArray convertedProcessedFrame = pEnv->NewByteArray(frameLength);
    pEnv->SetByteArrayRegion(convertedProcessedFrame, 0, frameLength, reinterpret_cast<jbyte*>(processedFrame));

    // return processed frame over JNI Bridge
    return convertedProcessedFrame;
}

// processFrame JNI handler
extern "C"
JNIEXPORT jbyteArray JNICALL
Java_biz_nycv_securitycamera_MotionDetector_processFrame(
        JNIEnv* pEnv,
        jobject pThis,
        jbyteArray sourceFrame,
        jint width,
        jint height,
        jbyteArray destinationFrame,
        jint stride) {

        // covert jbyteArray frames to cpp uint8_t*
        int frameLength = pEnv->GetArrayLength(sourceFrame);
        uint8_t* convertedSourceFrame = new uint8_t[frameLength];
        pEnv->GetByteArrayRegion(sourceFrame, 0, frameLength, reinterpret_cast<jbyte*>(convertedSourceFrame));

        uint8_t* convertedDestinationFrame = new uint8_t[frameLength];

        // perform process through fastcv filters
        uint8_t* processedFrame = motionDetector.processFrame(convertedSourceFrame, convertedSourceFrame, width, height, convertedDestinationFrame, stride);

        // cast uint8_t* frame back to jbyteArray
        jbyteArray convertedProcessedFrame = pEnv->NewByteArray(frameLength);
        pEnv->SetByteArrayRegion(convertedProcessedFrame, 0, frameLength, reinterpret_cast<jbyte*>(processedFrame));

        // return converted processed frame over JNI bridge
        return convertedProcessedFrame;
}

extern "C"
JNIEXPORT jint JNICALL
Java_biz_nycv_securitycamera_MotionDetector_calculateMotionArea(
        JNIEnv* pEnv,
        jobject pThis,
        jbyteArray sourceFrame,
        jint width,
        jint height) {

        // convert jbyteArray frame to cpp uint8_t*
        int frameLength = pEnv->GetArrayLength(sourceFrame);
        __android_log_print(ANDROID_LOG_DEBUG, "MotionDetector", "%i", frameLength);
        uint8_t* convertedSourceFrame = new uint8_t[frameLength];
        pEnv->GetByteArrayRegion(sourceFrame, 0, frameLength, reinterpret_cast<jbyte*>(convertedSourceFrame));

        __android_log_print(ANDROID_LOG_DEBUG, "MotionDetector", "before calcMotionArea");

//        uint8_t whitePixel = 255;
        // run area calculation on frame
        int motionArea = motionDetector.calculateMotionArea(convertedSourceFrame, width, height, frameLength, 255);

        __android_log_print(ANDROID_LOG_DEBUG, "MotionDetector", "after calcMotionArea");

        // return area back over JNI bridge
        return motionArea;
}

extern "C"
JNIEXPORT jint JNICALL
Java_biz_nycv_securitycamera_MotionDetector_checkMemoryBuffer(JNIEnv* pEnv, jobject pThis, jint motionArea) {
    int movingAverage = motionDetector.checkMemoryBuffer(motionArea);
    return movingAverage;
}

extern "C"
JNIEXPORT void JNICALL
Java_biz_nycv_securitycamera_MotionDetector_targetDSP(JNIEnv* pEnv, jobject pThis) {
    motionDetector.targetDSP();
    __android_log_print(ANDROID_LOG_DEBUG, "MotionDetector", "after target dsp");

}

extern "C"
JNIEXPORT jbyteArray JNICALL
Java_biz_nycv_securitycamera_MotionDetector_yuv420toRGB(
        JNIEnv* pEnv,
        jobject pThis,
        jbyteArray sourceFrame,
        jint width,
        jint height,
        jbyteArray destinationFrame) {

        // convert jbyteArray frame to cpp uint8_t*
        int frameLength = pEnv->GetArrayLength(sourceFrame);
        uint8_t* convertedSourceFrame = new uint8_t[frameLength];
        pEnv->GetByteArrayRegion(sourceFrame, 0, frameLength, reinterpret_cast<jbyte*>(convertedSourceFrame));

        // create empty destination container !!! 32 bit
        int destFrameLength = pEnv->GetArrayLength(destinationFrame);
        uint32_t* convertedDestinationFrame = new uint32_t[frameLength / 4];

        // hit fastcv fn
        uint32_t* rgbFrame = motionDetector.yuv420toRGB(convertedSourceFrame, width, height, convertedDestinationFrame);

        // convert rgb frame uint32_t* back into jByteArray
        int rgbFrameLength = sizeof(rgbFrame);
        jbyteArray convertedRgbFrame = pEnv->NewByteArray(rgbFrameLength);

//          pEnv->SetByteArrayRegion(convertedRgbFrame, 0, rgbFrameLength, reinterpret_cast<jbyte*>(convertedRgbFrame));
        pEnv->SetByteArrayRegion(convertedRgbFrame, 0, rgbFrameLength, reinterpret_cast<jbyte*>(rgbFrame));

        // return the converted rgb frame as jbyteArray over JNI bridge
        return convertedRgbFrame;
}




